#include "SnakeElementBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"
#include "SnakeGameModeBase.h"

ASnakeElementBase::ASnakeElementBase()
{
	PrimaryActorTick.bCanEverTick = false;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

}

void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();
}

void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASnakeElementBase::Interact(AActor* Interactable, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactable);
	if (IsValid(Snake))
	{
		auto GameMode = Cast<ASnakeGameModeBase>(GetWorld()->GetAuthGameMode());
		if (IsValid(GameMode))
			GameMode->LoseGame();
		Snake->Destroy();
	}
}

void ASnakeElementBase::SetFirstElementType_Implementation()
{
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
}

void ASnakeElementBase::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (IsValid(SnakeOwner))
	{
		SnakeOwner->SnakeElementOverlap(this, OtherActor);
	}
}

void ASnakeElementBase::ToggleCollision() const
{
	if (MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	else
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void ASnakeElementBase::ToggleVisibility() const
{
	MeshComponent->ToggleVisibility();
}

bool ASnakeElementBase::GetVisibility() const
{
	return MeshComponent->IsVisible();
}



