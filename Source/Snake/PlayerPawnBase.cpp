// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "SnakeBase.h"

APlayerPawnBase::APlayerPawnBase()
{
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Pawn Camera"));
	RootComponent = PawnCamera;
	
}

void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));

	CreateSnakeActor();
}

void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVercticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::HandlePlayerVercticalInput(float Value)
{
	if (IsValid(SnakeActor))
	{
		if (SnakeActor->GetMove())
		{
			if (Value > 0 && SnakeActor->LastDirection != EMovementDirection::DOWN)
			{
				SnakeActor->LastDirection = EMovementDirection::UP;
				SnakeActor->SetMove(false);
			}
			else if (Value < 0 && SnakeActor->LastDirection != EMovementDirection::UP)
			{
				SnakeActor->LastDirection = EMovementDirection::DOWN;
				SnakeActor->SetMove(false);
			}
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float Value)
{
	if (IsValid(SnakeActor))
	{
		if (SnakeActor->GetMove())
		{
			if (Value > 0 && SnakeActor->LastDirection != EMovementDirection::LEFT)
			{
				SnakeActor->LastDirection = EMovementDirection::RIGHT;
				SnakeActor->SetMove(false);
			}
			else if (Value < 0 && SnakeActor->LastDirection != EMovementDirection::RIGHT)
			{
				SnakeActor->LastDirection = EMovementDirection::LEFT;
				SnakeActor->SetMove(false);
			}
		}
	}
}

