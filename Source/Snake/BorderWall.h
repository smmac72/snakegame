// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "BorderWall.generated.h"

class UStaticMeshComponent;
UCLASS()
class SNAKE_API ABorderWall : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	ABorderWall();
	
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	virtual void Interact(AActor* Interactable, bool bIsHead) override;
};
