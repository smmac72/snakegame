#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	RIGHT,
	LEFT
};
UCLASS()
class SNAKE_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY()
	float CurrentSpeed;
	
	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
	EMovementDirection LastDirection = EMovementDirection::DOWN;
protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementCount = 1, bool InitialAdd = false);

	UFUNCTION(BlueprintCallable)
	void Move();

	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
	
	void SetMove(bool NewSet);

	bool GetMove() const;
private:
	bool MoveCompleted = false;
};
