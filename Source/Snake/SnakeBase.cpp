#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "SnakeGameModeBase.h"

ASnakeBase::ASnakeBase()
{
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
}

void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	AddSnakeElement(3, true);
	CurrentSpeed = Cast<ASnakeGameModeBase>(AActor::GetWorld()->GetAuthGameMode())->Speed;
}

void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CurrentSpeed = Cast<ASnakeGameModeBase>(AActor::GetWorld()->GetAuthGameMode())->Speed;
	SetActorTickInterval(CurrentSpeed);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementCount, bool InitialAdd)
{
	for (int i = 0; i < ElementCount; i++)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->SnakeOwner = this;
		const int32 ElemIndex = SnakeElements.Add(NewSnakeElement);
		if (ElemIndex == 0)
			NewSnakeElement->SetFirstElementType();
		if (!InitialAdd)
			NewSnakeElement->ToggleVisibility();
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);

	switch (LastDirection)
	{
		case EMovementDirection::UP:
			MovementVector.X += ElementSize;
			break;
		case EMovementDirection::DOWN:
			MovementVector.X -= ElementSize;
			break;
		case EMovementDirection::RIGHT:
			MovementVector.Y += ElementSize;
			break;
		case EMovementDirection::LEFT:
			MovementVector.Y -= ElementSize;
			break;
	}

	SnakeElements[0]->ToggleCollision();
	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		const auto PreviousElement = SnakeElements[i - 1];

		FVector PreviousLocation = PreviousElement->GetActorLocation();
		CurrentElement->SetActorLocation(PreviousLocation);

		if (!CurrentElement->GetVisibility())
			CurrentElement->ToggleVisibility();
	}
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	
	SnakeElements[0]->ToggleCollision();

	SetMove(true);
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		const bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
			InteractableInterface->Interact(this, bIsFirst);
	}
}

void ASnakeBase::SetMove(bool NewSet)
{
	MoveCompleted = NewSet;
}

bool ASnakeBase::GetMove() const
{
	return MoveCompleted;
}

