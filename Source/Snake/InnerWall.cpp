

#include "InnerWall.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeGameModeBase.h"

AInnerWall::AInnerWall()
{
	PrimaryActorTick.bCanEverTick = false;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Obstacle"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void AInnerWall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AInnerWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AInnerWall::Interact(AActor* Interactable, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactable);
	if (IsValid(Snake))
	{
		auto GameMode = Cast<ASnakeGameModeBase>(GetWorld()->GetAuthGameMode());
		if (IsValid(GameMode))
		{
			if (GameMode->GetSpawnCheckingState())
				GameMode->SetFreeToSpawnState(false);
			else
			{
				if (GameMode->GetDisableObstaclesTime() == 0)
				{
					GameMode->LoseGame();
					Snake->Destroy();
				}
			}
		}
	}
}

void AInnerWall::ToggleObstacleVisibility() const
{
	MeshComponent->ToggleVisibility();
}


