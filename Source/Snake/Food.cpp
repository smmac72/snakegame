#include "Food.h"
#include "SnakeBase.h"
#include "SnakeGameModeBase.h"

AFood::AFood()
{
	PrimaryActorTick.bCanEverTick = false;
}

void AFood::BeginPlay()
{
	Super::BeginPlay();
}

void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFood::Interact(AActor* Interactable, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactable);
		if (IsValid(Snake))
		{
			auto GameMode = Cast<ASnakeGameModeBase>(GetWorld()->GetAuthGameMode());
			if (IsValid(GameMode))
			{	
				Snake->AddSnakeElement((GameMode->GetBonusDoubleScoreTime() != 0) ? 2 : 1); // костыль потому что я не смог закастовать змейку в гейммоде если что
				GameMode->AddScore();
				if (IsBonus)
				{
					const int32 BonusType = static_cast<int32>(round(FMath::FRandRange(0, 3)));
					switch (BonusType)
					{
					case 0:
						GameMode->BonusDisableObstacles();
						break;
					case 1:
						GameMode->BonusDoubleScore();
						break;
					case 2:
						GameMode->BonusExtraPoints();
						break;
					case 3:
						GameMode->BonusSpeed();
						break;
					default:
						break;
					}
				}
				GameMode->ChangeFood(-1);
				GameMode->DecreaseObstacleRequirement();
			}
			Destroy();
		}
	}
}

void AFood::ToggleBonus()
{
	IsBonus = true;
}

