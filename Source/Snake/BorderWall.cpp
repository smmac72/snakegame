#include "BorderWall.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeGameModeBase.h"

ABorderWall::ABorderWall()
{
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wall"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

void ABorderWall::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABorderWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABorderWall::Interact(AActor* Interactable, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactable);
	if (IsValid(Snake))
	{
		auto GameMode = Cast<ASnakeGameModeBase>(GetWorld()->GetAuthGameMode());
		if (IsValid(GameMode))
			GameMode->LoseGame();
		Snake->Destroy();
	}
}

