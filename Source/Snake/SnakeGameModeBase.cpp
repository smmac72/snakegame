// Copyright Epic Games, Inc. All Rights Reserved.


#include "SnakeGameModeBase.h"
#include "Food.h"
#include "InnerWall.h"

void ASnakeGameModeBase::LoseGame()
{
	LoseState = true;
}


void ASnakeGameModeBase::AddScore(int32 AddedScore)
{
	Score += (DoubleScoreTime != 0 ? AddedScore * 2 : AddedScore);
}


void ASnakeGameModeBase::SpawnFood()
{
	const int32 MinRange = (FoodAmount == 0 ? 1 : 0);
	for (int32 i = 0; i < static_cast<int32>(round(FMath::FRandRange(MinRange, 2))); i++)
	{
		FVector NewLocation(FMath::FRandRange(MinX, MaxX), FMath::FRandRange(MinY, MaxY), 40);
		FTransform NewTransform(NewLocation);
		AFood* NewFood = GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
		ChangeFood(1);

		if (FMath::FRandRange(0, 100) < BonusRarity)
			NewFood->ToggleBonus();
	}
}

void ASnakeGameModeBase::ChangeFood(int32 Side)
{
	FoodAmount += Side;
	if (Side < 0)
		SpawnFood();
}

void ASnakeGameModeBase::BonusDisableObstacles()
{
	DisableObstaclesTime = GetWorld()->GetTimerManager().GetTimerRemaining(BonusSpeedTimer) + 15;
	GetWorld()->GetTimerManager().SetTimer(BonusDisableObstaclesTimer, this, &ASnakeGameModeBase::OnEndBonusDisableObstacles, DisableObstaclesTime, true);
}

void ASnakeGameModeBase::BonusDoubleScore()
{
	DoubleScoreTime = GetWorld()->GetTimerManager().GetTimerRemaining(BonusSpeedTimer) + 10;
	GetWorld()->GetTimerManager().SetTimer(BonusDoubleScoreTimer, this, &ASnakeGameModeBase::OnEndBonusDoubleScore, DoubleScoreTime, true);
}

void ASnakeGameModeBase::BonusExtraPoints()
{
	const int32 RandomScore = static_cast<int32>(round(FMath::FRandRange(1, 5)));
	AddScore(RandomScore);
}

void ASnakeGameModeBase::BonusSpeed()
{
	SpeedTime = GetWorld()->GetTimerManager().GetTimerRemaining(BonusSpeedTimer) + 15;
	GetWorld()->GetTimerManager().SetTimer(BonusSpeedTimer, this, &ASnakeGameModeBase::OnEndBonusSpeed, SpeedTime, true);
	SetSpeed(true);
}

void ASnakeGameModeBase::OnEndBonusDisableObstacles()
{
	DisableObstaclesTime = 0;
	GetWorld()->GetTimerManager().ClearTimer(BonusDisableObstaclesTimer);
}

void ASnakeGameModeBase::OnEndBonusDoubleScore()
{
	DoubleScoreTime = 0;
	GetWorld()->GetTimerManager().ClearTimer(BonusDoubleScoreTimer);
}

void ASnakeGameModeBase::OnEndBonusSpeed()
{
	SpeedTime = 0;
	GetWorld()->GetTimerManager().ClearTimer(BonusSpeedTimer);
	SetSpeed(false);
}

void ASnakeGameModeBase::SetSpeed(bool Bonus)
{
	if (Bonus)
	{
		if (!SpeedBonusActive)
		{
			Speed /= 2;
			SpeedBonusActive = true;
		}
	}
	else
	{
		if (SpeedBonusActive)
		{
			Speed *= 2;
			SpeedBonusActive = false;
		}
	}
}

float ASnakeGameModeBase::GetDisableObstaclesTime() const
{
	return DisableObstaclesTime;
}

float ASnakeGameModeBase::GetBonusDoubleScoreTime() const
{
	return DoubleScoreTime;
}

float ASnakeGameModeBase::GetBonusSpeedTime() const
{
	return SpeedTime;
}

auto ASnakeGameModeBase::DecreaseObstacleRequirement() -> void
{
	CurrentObstacleRequirement--;
	if (!CurrentObstacleRequirement)
	{
		SpawnObstacle();
		RestoreObstacleRequirement();
	}
}

void ASnakeGameModeBase::RestoreObstacleRequirement()
{
	if (InitialObstacleRequirement > Minimal_Obstacle_Requirement)
		CurrentObstacleRequirement = --InitialObstacleRequirement;
	else
		CurrentObstacleRequirement = InitialObstacleRequirement;
}

int32 ASnakeGameModeBase::GetObstacleRequirement() const
{
	return CurrentObstacleRequirement;
}

void ASnakeGameModeBase::SpawnObstacle()
{
	AInnerWall* NewObstacle;
	do
	{
		FVector NewLocation(FMath::FRandRange(MinX, MaxX), FMath::FRandRange(MinY, MaxY), 50);
		FTransform NewTransform(NewLocation);
		NewObstacle = GetWorld()->SpawnActor<AInnerWall>(ObstacleClass, NewTransform);
		SpawnChecking = true;
		NewObstacle->ToggleObstacleVisibility();
	}
	while (!FreeToSpawn);
	NewObstacle->ToggleObstacleVisibility();
	SpawnChecking = false;
}

bool ASnakeGameModeBase::GetSpawnCheckingState() const
{
	return SpawnChecking;
}

bool ASnakeGameModeBase::GetFreeToSpawnState() const
{
	return FreeToSpawn;
}

void ASnakeGameModeBase::SetFreeToSpawnState(bool NewState)
{
	FreeToSpawn = NewState;
}
