#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameModeBase.generated.h"

class AFood;
class AInnerWall;
UCLASS()
class SNAKE_API ASnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	// states
	auto LoseGame() -> void;
	UPROPERTY(BlueprintReadWrite)
	bool LoseState = false;

	// game vars
	UPROPERTY()
	float Speed = 0.2;
	UPROPERTY(BlueprintReadOnly)
	int32 Score = 0;
	
	///////// score /////////
	UFUNCTION()
	void AddScore(int32 AddedScore = 1);

	////////// food //////////
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodClass;

	UFUNCTION()
	void ChangeFood(int32 Side); // -1 or 1
	
	UFUNCTION()
	void BonusDisableObstacles();
	UFUNCTION()
	void BonusDoubleScore();
	UFUNCTION()
	void BonusExtraPoints();
	UFUNCTION()
	void BonusSpeed();

	UFUNCTION()
	void OnEndBonusDisableObstacles();
	UFUNCTION()
	void OnEndBonusDoubleScore();
	UFUNCTION()
	void OnEndBonusSpeed();
	
	UFUNCTION()
	void SetSpeed(bool Bonus);

	UFUNCTION()
	float GetDisableObstaclesTime() const;
	UFUNCTION()
	float GetBonusDoubleScoreTime() const;
	UFUNCTION()
	float GetBonusSpeedTime() const;

	UPROPERTY(BlueprintReadOnly)
	FTimerHandle BonusDisableObstaclesTimer;
	UPROPERTY(BlueprintReadOnly)
	FTimerHandle BonusDoubleScoreTimer;
	UPROPERTY(BlueprintReadOnly)
	FTimerHandle BonusSpeedTimer;

	//////// obstacles ////////
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AInnerWall> ObstacleClass;

	UFUNCTION()
	void DecreaseObstacleRequirement();
	UFUNCTION()
	void RestoreObstacleRequirement();
	UFUNCTION()
	int32 GetObstacleRequirement() const;

	UFUNCTION()
	void SpawnObstacle();

	UFUNCTION()
	bool GetSpawnCheckingState() const;
	UFUNCTION()
	bool GetFreeToSpawnState() const;

	UFUNCTION()
	void SetFreeToSpawnState(bool NewState);
private:
	
	/////// field //////////
	const int32 MinX = -900;
	const int32 MaxX = 900;
	const int32 MinY = -1290;
	const int32 MaxY = 1060;
	
	//////// food ////////
	UPROPERTY()
	int32 FoodAmount = 1;
	
	UPROPERTY(EditDefaultsOnly)
	int32 BonusRarity = 10;
	void SpawnFood();

	UPROPERTY()
	bool SpeedBonusActive = false;

	UPROPERTY()
	float DisableObstaclesTime = 0;
	UPROPERTY()
	float DoubleScoreTime = 0;
	UPROPERTY()
	float SpeedTime = 0;
	
	//////// obstacles ////////
	const int32 Default_Obstacle_Requirement = 20;
	const int32 Minimal_Obstacle_Requirement = 10;

	bool SpawnChecking = false;
	bool FreeToSpawn = true;
	
	int32 InitialObstacleRequirement = Default_Obstacle_Requirement;
	int32 CurrentObstacleRequirement = InitialObstacleRequirement;
};
